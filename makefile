DOCKER-COMPOSE = docker compose --file docker/docker-compose.dev.yaml



#----------------------------------------------------------------------------------------------------------------------#
# Environment                                                                                                          #
#----------------------------------------------------------------------------------------------------------------------#

up: ## Set up containers
	@${DOCKER-COMPOSE} up --detach --build --remove-orphans

down: ## Destroy containers
	@${DOCKER-COMPOSE} down --volumes --remove-orphans

start: ## Start containers
	@${DOCKER-COMPOSE} start

stop: ## Stop containers
	@${DOCKER-COMPOSE} stop

logs: ## Show containers logs
	@${DOCKER-COMPOSE} logs --follow

shell: ## Open the app container's shell
	@${DOCKER-COMPOSE} exec app fish


#----------------------------------------------------------------------------------------------------------------------#
# Project                                                                                                              #
#----------------------------------------------------------------------------------------------------------------------#

all: up ## Set up containers and start the project

build: ## Build dist
	${DOCKER-COMPOSE} exec app fish -c "npm run build"

run-dist: ## Preview dist build
	${DOCKER-COMPOSE} exec app fish -c "npm run preview"


#----------------------------------------------------------------------------------------------------------------------#
# Other                                                                                                                #
#----------------------------------------------------------------------------------------------------------------------#

help: ## Display available targets
	@awk \
		'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[32m<target>\033[0m\n"; } \
		END { printf "\n"; } \
		/^#[^#-]/ { printf "\n \033[1m%s\033[0m\n", substr($$0, 2, length($$0)-6); } \
		/^[a-zA-Z@_-]+:.*?##/ { printf "    \033[32m%-15s\033[0m %s\n", $$1, $$2; }' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
