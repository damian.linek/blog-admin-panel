
import { Admin, Resource } from 'react-admin';
import { AuthorList, AuthorShow, AuthorEdit } from "./resources/author"
import { CategoryList, CategoryShow, CategoryEdit } from "./resources/category"
import { TagList, TagShow, TagEdit } from "./resources/tag"
import { PostList, PostShow, PostEdit } from "./resources/post"
import { Dashboard } from "./dashboard";
import { DataProvider } from "./dataProvider";
import PersonIcon from '@mui/icons-material/Person';
import CategoryIcon from '@mui/icons-material/Category';
import TagIcon from '@mui/icons-material/Tag';
import ArticleIcon from '@mui/icons-material/Article';

export const App = () => 
  <Admin dataProvider={DataProvider} dashboard={Dashboard}>
    <Resource name="author" list={AuthorList} show={AuthorShow} edit={AuthorEdit} icon={PersonIcon} recordRepresentation={(record) => `${record.firstname} ${record.lastname}`}/>
    <Resource name="category" list={CategoryList} show={CategoryShow} edit={CategoryEdit} icon={CategoryIcon} recordRepresentation={(record) => record.title}/>
    <Resource name="tag" list={TagList} show={TagShow} edit={TagEdit} icon={TagIcon} recordRepresentation={(record) => record.title}/>
    <Resource name="post" list={PostList} show={PostShow} edit={PostEdit} icon={ArticleIcon}/>
  </Admin>;