import { fetchUtils } from "react-admin";
import queryString from "query-string";

const apiUrl = import.meta.env.VITE_API_URL;

export const DataProvider = {
    getList: (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const filters = DataTransformer.transformFilters(params.filter);

        const query = {
            sort: JSON.stringify({field: field, order: order}),
            pagination: JSON.stringify({page: page, limit: perPage}),
            filters: JSON.stringify(filters),
        };

        const url = `${apiUrl}/${resource}/?${queryString.stringify(query)}`;

        return fetchUtils.fetchJson(url).then(({ headers, json }) => ({
            data: json.data,
            total: json.total
        }));
    },
    getOne: (resource, params) =>
        fetchUtils.fetchJson(`${apiUrl}/${resource}/${params.id}`).then(({ json }) => ({
            data: json,
        })),
    getMany: (resource, params) => {
        const filters = DataTransformer.transformIds(params.ids);

        const query = {
            pagination: JSON.stringify({page: 1, limit: 100}),
            filters: JSON.stringify(filters),
        };

        const url = `${apiUrl}/${resource}/?${queryString.stringify(query)}`;

        return fetchUtils.fetchJson(url).then(({ headers, json }) => ({
            data: json.data,
            total: json.total
        }));
    }
};

const DataTransformer = {
    transformFilters: (filters) => {
        return filters;
    },
    transformIds: (ids) => {
        return {'uuid': ids};
    },
}
