import { 
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
  DateTimeInput,
} from "react-admin";
import { EditToolbar } from "./toolbar"
import { Divider } from '@mui/material';

const filters = [];
const sort = { field: 'createdAt', order: 'ASC'}

export const TagList = () => (
  <List filters={filters} sort={sort}>
    <Datagrid rowClick="show">
      <TextField source="id" />
      <TextField source="title" />
      <EditButton />
    </Datagrid>
  </List>
);

export const TagShow = () => (
  <Show>
    <SimpleShowLayout sx={{ width: {xs: 1, md: 2/3, lg: 1/2} }} divider={<Divider flexItem/>}>
      <TextField source="id"/>
      <TextField source="title"/>
      <DateField source="createdAt" showTime/>
      <DateField source="updatedAt" showTime/>
    </SimpleShowLayout>
  </Show>
);

export const TagEdit = () => (
  <Show>
    <SimpleForm sx={{ width: {xs: 1, md: 2/3, lg: 1/2} }} toolbar={<EditToolbar/>}>
      <TextInput source="id" disabled fullWidth={true}/>
      <TextInput source="title" fullWidth={true} />
      <DateTimeInput source="createdAt" disabled={true} fullWidth={true} />
      <DateTimeInput source="updatedAt" disabled={true} fullWidth={true} />
    </SimpleForm>
  </Show>
);