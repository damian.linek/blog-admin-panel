import { 
  List,
  Datagrid,
  TextField,
  DateField,
  EditButton,
  Show,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
  DateTimeInput,
} from "react-admin";
import { EditToolbar } from "./toolbar"
import { Divider } from '@mui/material';

const filters = [];
const sort = { field: 'createdAt', order: 'ASC'}

export const AuthorList = () => (
  <List filters={filters} sort={sort}>
    <Datagrid rowClick="show">
      <TextField source="id" />
      <TextField source="firstname" />
      <TextField source="lastname" />
      <EditButton />
    </Datagrid>
  </List>
);

export const AuthorShow = () => (
  <Show>
    <SimpleShowLayout sx={{ width: {xs: 1, md: 2/3, lg: 1/2} }} divider={<Divider flexItem/>}>
      <TextField source="id"/>
      <TextField source="firstname"/>
      <TextField source="lastname"/>
      <TextField source="shortDesc"/>
      <DateField source="createdAt" showTime/>
      <DateField source="updatedAt" showTime/>
    </SimpleShowLayout>
  </Show>
);

export const AuthorEdit = () => (
  <Show>
    <SimpleForm sx={{ width: {xs: 1, md: 2/3, lg: 1/2} }} toolbar={<EditToolbar/>}>
      <TextInput source="id" disabled fullWidth={true}/>
      <TextInput source="firstname" fullWidth={true} />
      <TextInput source="lastname" fullWidth={true} />
      <TextInput source="shortDesc" fullWidth={true} multiline={true}/>
      <DateTimeInput source="createdAt" disabled={true} fullWidth={true} />
      <DateTimeInput source="updatedAt" disabled={true} fullWidth={true} />
    </SimpleForm>
  </Show>
);