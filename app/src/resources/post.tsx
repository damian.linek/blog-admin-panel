import { 
  List,
  Datagrid,
  TextField,
  ReferenceField,
  DateField,
  ReferenceArrayField,
  EditButton,
  Show,
  SimpleShowLayout,
  SimpleForm,
  TextInput,
  DateTimeInput,
  ReferenceInput,
  ReferenceArrayInput,
  AutocompleteInput,
  AutocompleteArrayInput,
} from "react-admin";
import { EditToolbar } from "./toolbar"
import { Divider } from '@mui/material';

const filters = [];
const sort = { field: 'createdAt', order: 'ASC'}

export const PostList = () => (
  <List filters={filters} sort={sort}>
    <Datagrid rowClick="show">
      <TextField source="id" />
      <TextField source="title" />
      <TextField source="shortDesc" />
      <ReferenceField source="authorUuid" label="Author" reference="author"/>
      <ReferenceField source="categoryUuid" label="Category" reference="category"/>
      <EditButton />
    </Datagrid>
  </List>
);

export const PostShow = () => (
  <Show>
    <SimpleShowLayout sx={{ width: {xs: 1, md: 2/3, lg: 1/2}}} divider={<Divider flexItem/>}>
      <TextField source="id"/>
      <TextField source="title"/>
      <TextField source="shortDesc"/>
      <TextField source="description"/>
      <ReferenceField source="authorUuid" label="Author" reference="author"/>
      <ReferenceField source="categoryUuid" label="Category" reference="category"/>
      <ReferenceArrayField source="tagUuidList" label="Tag" reference="tag"/>
      <DateField source="createdAt" showTime/>
      <DateField source="updatedAt" showTime/>
    </SimpleShowLayout>
  </Show>
);

export const PostEdit = () => (
  <Show>
    <SimpleForm sx={{ width: {xs: 1, md: 2/3, lg: 1/2} }} toolbar={<EditToolbar/>}>
      <TextInput source="id" disabled fullWidth={true}/>
      <TextInput source="title" fullWidth={true}/>
      <TextInput source="shortDesc" fullWidth={true} multiline={true}/>
      <TextInput source="description" fullWidth={true} multiline={true}/>
      <ReferenceInput source="authorUuid" reference="author" sort={{ field: 'lastname', 'order': 'ASC'}} perPage={100}>
        <AutocompleteInput fullWidth={true} label="Author"/>
      </ReferenceInput>
      <ReferenceInput source="categoryUuid" reference="category" sort={{ field: 'title', 'order': 'ASC'}} perPage={100}>
        <AutocompleteInput fullWidth={true} label="Category"/>
      </ReferenceInput>
      <ReferenceArrayInput source="tagUuidList" reference="tag" sort={{ field: 'title', 'order': 'ASC'}} perPage={100}>
        <AutocompleteArrayInput fullWidth={true} label="Tag"/>
      </ReferenceArrayInput>
      <DateTimeInput source="createdAt" disabled={true} fullWidth={true}/>
      <DateTimeInput source="updatedAt" disabled={true} fullWidth={true}/>
    </SimpleForm>
  </Show>
);