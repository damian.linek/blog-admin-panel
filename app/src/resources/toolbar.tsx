import { SaveButton, DeleteButton, Toolbar } from 'react-admin';

export const EditToolbar = props => {
    return (
        <Toolbar {...props}>
            <SaveButton />
        </Toolbar>
    );
  };

export const ShowToolbar = props => {
    return (
        <Toolbar {...props}>
            <SaveButton disabled/>
            <DeleteButton disabled/>
        </Toolbar>
    );
  };
