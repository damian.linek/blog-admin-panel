# blog-admin

blog-admin is a User Interface part of the blog-symfony-ddd Project.  
It is based on [Nuxt.js](https://nuxtjs.org).


## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed [Docker Engine](https://docs.docker.com/engine/) (v20.10 or higher)
* You have installed [Docker Compose](https://docs.docker.com/compose/) (v2.2 or higher)
* You have installed [GNU Make](https://www.gnu.org/software/make/)

## Structure

The project root directory consists of two subdirectories:

* app - contains application codebase
* docker - contains container environment configuration

## Installing

To set up the project in `dev` environment, follow these steps:

```
make all
```

## Using

To use the project, follow these steps:

```
make help
```
